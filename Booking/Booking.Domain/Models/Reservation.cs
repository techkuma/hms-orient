﻿using System;
using System.Collections.Generic;

namespace Booking.Domain.Models
{
    public class Reservation : Entity
    {
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public ReservationStatus Status { get; set; }
        public Guid PropertyID { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        public DateTime BookingDate { get; set; }
        public int NumberOfRooms { get; set; }
        public DateTime? CancellationDate { get; set; }
        public int NumberOfAdults { get; set; }
        public string RoomConfirmationNumber { get; set; }
        public string ItineraryNumber { get; set; }
        public decimal Total { get; set; }
        public string CurrencyCode { get; set; }
        public string PromotionCode { get; set; }
        public Guid DistributionChannel { get; set; }
        public TravelType TravelType { get; set; }
        public virtual ReservationPropertyInfo PropertyInfo { get; set; }

        //Support search fields
        public string ProfileNames { get; set; }
        public string ProfileEmails { get; set; }
        public string ProfilePhones { get; set; }
        public string ProfileReferenceIDs { get; set; }
        public string RoomTypeCodes { get; set; }
        public string RoomTypeNames { get; set; }
        public string RatePlanCodes { get; set; }
        public string RatePlanNames { get; set; }
        public string ReservationReferenceIDs { get; set; }
        public int NumberOfOtherOccupancies { get; set; }

        public virtual ICollection<ReservationTax> Taxes { get; set; }
        public virtual ICollection<ReservationFee> Fees { get; set; }
        public virtual ICollection<ReservationCancellationInfo> CancellationInfos { get; set; }
        public virtual ICollection<ReservationCancellationPolicy> CancellationPolicies { get; set; }
        public virtual ICollection<ReservationGuaranteeInfo> GuaranteeInfos { get; set; }
        public virtual ICollection<ReservationGuaranteePolicy> GuaranteePolicies { get; set; }
        public virtual ICollection<ReservationOtherOccupancy> OtherOccupancies { get; set; }
        public virtual ICollection<ReservationReferenceID> ReferenceIds { get; set; }
        public virtual ICollection<ReservationRequest> Requests { get; set; }
        public virtual ICollection<ReservationLog> Logs { get; set; }
        public virtual ICollection<ReservationExtra> Extras { get; set; }
        public virtual ICollection<ReservationProfile> Profiles { get; set; }
        public virtual ICollection<ReservationRoomRate> RoomRates { get; set; }
    }
}
