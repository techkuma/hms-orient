﻿using AutoMapper;
using Booking.Business.Dtos;
using Booking.Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;

namespace Booking.API
{
    public static class AutoMapperConfig
    {
        public static void AddAutoMapperWithConfig(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                AddConfigs(cfg);
            }, AppDomain.CurrentDomain.GetAssemblies());
        }

        private static void AddConfigs(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ReservationDto, Reservation>()
                .ForMember(dest => dest.NumberOfAdults, opt => opt.MapFrom(s => s.RoomOccupancy.NumberOfAdult))
                .ForMember(dest => dest.Total, opt => opt.MapFrom(s => s.Total.Amount.Amount))
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(s => s.Total.Amount.CurrencyCode))
                .ForMember(dest => dest.DistributionChannel, opt => opt.MapFrom(s => Guid.NewGuid()))
                .ReverseMap()
                .ForPath(dest => dest.RoomOccupancy.NumberOfAdult, opt => opt.MapFrom(s => s.NumberOfAdults))
                .ForPath(dest => dest.Total.Amount.Amount, opt => opt.MapFrom(s => s.Total))
                .ForPath(dest => dest.Total.Amount.CurrencyCode, opt => opt.MapFrom(s => s.CurrencyCode))
                .ForPath(dest => dest.RoomOccupancy.OtherOccupancies, opt => opt.MapFrom(s => s.OtherOccupancies))              
                ;

            cfg.CreateMap<PropertyInfoDto, ReservationPropertyInfo>()
                .ForMember(dest => dest.JsonValue, opt => opt.MapFrom(s => JsonConvert.SerializeObject(s)))
                .ReverseMap()
                .ForMember(dest => dest.Code, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Name))
                .ForMember(dest => dest.Slug, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Slug))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Description))
                .ForMember(dest => dest.CountryID, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).CountryID))
                .ForMember(dest => dest.CountryStateID, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).CountryStateID))
                .ForMember(dest => dest.HotelTypeID, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).HotelTypeID))
                .ForMember(dest => dest.City, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).City))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Street))
                .ForMember(dest => dest.Telephone, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Telephone))
                .ForMember(dest => dest.Fax, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Fax))
                .ForMember(dest => dest.WebAddress, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).WebAddress))
                .ForMember(dest => dest.LocationCoordinates, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).LocationCoordinates))
                .ForMember(dest => dest.Star, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).Star))
                .ForMember(dest => dest.CheckInTime, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).CheckInTime))
                .ForMember(dest => dest.CheckOutTime, opt => opt.MapFrom(s => JsonConvert.DeserializeObject<PropertyInfoDto>(s.JsonValue).CheckOutTime))
                ; 

            cfg.CreateMap<RoomRateDto, ReservationRoomRate>()
                .ForMember(dest => dest.RateAmount, opt => opt.MapFrom(s => s.RateAmount.Amount.Amount))
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ReverseMap()
                ; 

            cfg.CreateMap<RoomTypeInfoDto, ReservationRoomTypeInfo>()
                .ForMember(dest => dest.JsonValue, opt => opt.MapFrom(s => JsonConvert.SerializeObject(s)))
                .ReverseMap()
                ; 

            cfg.CreateMap<ProfileDto, ReservationProfile>()
                .ForMember(dest => dest.PassportID, opt => opt.MapFrom(s => s.Passport.ID))
                .ForMember(dest => dest.PassportExpiredDate, opt => opt.MapFrom(s => s.Passport.ExpiredDate))
                .ForMember(dest => dest.City, opt => opt.MapFrom(s => s.Address))
                .ForMember(dest => dest.State, opt => opt.MapFrom(s => s.Address))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(s => s.Address))
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ReverseMap()
                ; 

            cfg.CreateMap<GuaranteeInfoDto, ReservationGuaranteeInfo>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ReverseMap(); 
        }
    }
}
