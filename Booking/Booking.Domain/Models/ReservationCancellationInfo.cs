﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationCancellationInfo : Entity
    {
        public Guid ReservationID { get; set; }
        public string CancellationPolicyRefID { get; set; }
        public string CancellationPolicyName { get; set; }
        public bool IsRefundable { get; set; }
        public decimal? CancelPenaltyAmount { get; set; }
        public decimal? CancelPenaltyPercent { get; set; }
        public int CancelPenaltyNumberNights { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
