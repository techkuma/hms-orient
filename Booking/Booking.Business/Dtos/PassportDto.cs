﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Business.Dtos
{
    public class PassportDto
    {
        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("expiredDate")]
        public DateTime ExpiredDate { get; set; }
    }
}
