﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationRequest : Entity
    {
        public Guid ReservationID { get; set; }
        public RequestType RequestType { get; set; }
        public string RequestContent { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
