﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using Booking.Domain.Models;
namespace Booking.Infrastructure.Context
{
    public partial class BookingContext : DbContext
    {

        public BookingContext(DbContextOptions<BookingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<ReservationCancellationInfo> ReservationCancellationInfo { get; set; }
        public virtual DbSet<ReservationCancellationPolicy> ReservationCancellationPolicy { get; set; }
        public virtual DbSet<ReservationExtra> ReservationExtra { get; set; }
        public virtual DbSet<ReservationExtraFee> ReservationExtraFee { get; set; }
        public virtual DbSet<ReservationExtraTax> ReservationExtraTax { get; set; }
        public virtual DbSet<ReservationFee> ReservationFee { get; set; }
        public virtual DbSet<ReservationGuaranteeInfo> ReservationGuaranteeInfo { get; set; }
        public virtual DbSet<ReservationGuaranteePolicy> ReservationGuaranteePolicy { get; set; }
        public virtual DbSet<ReservationLog> ReservationLog { get; set; }
        public virtual DbSet<ReservationOtherOccupancy> ReservationOtherOccupancy { get; set; }
        public virtual DbSet<ReservationProfile> ReservationProfile { get; set; }
        public virtual DbSet<ReservationProfileReferenceID> ReservationProfileReferenceID { get; set; }
        public virtual DbSet<ReservationPropertyInfo> ReservationPropertyInfo { get; set; }
        public virtual DbSet<ReservationRatePlanInfo> ReservationRatePlanInfo { get; set; }
        public virtual DbSet<ReservationReferenceID> ReservationReferenceID { get; set; }
        public virtual DbSet<ReservationRequest> ReservationRequest { get; set; }
        public virtual DbSet<ReservationRoomRate> ReservationRoomRate { get; set; }
        public virtual DbSet<ReservationRoomRateFee> ReservationRoomRateFee { get; set; }
        public virtual DbSet<ReservationRoomRateTax> ReservationRoomRateTax { get; set; }
        public virtual DbSet<ReservationRoomTypeAmenityInfo> ReservationRoomTypeAmenityInfo { get; set; }
        public virtual DbSet<ReservationRoomTypeInfo> ReservationRoomTypeInfo { get; set; }
        public virtual DbSet<ReservationTax> ReservationTax { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.ToTable("Reservation", "HmsOrient");

                entity.HasIndex(e => e.ItineraryNumber)
                    .HasName("ItineraryIndex");

                entity.HasIndex(e => e.RoomConfirmationNumber)
                    .HasName("RoomConfirmationNumberIndex");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasColumnType("char(3)");

                entity.Property(e => e.DistributionChannel)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.Property(e => e.ItineraryNumber)
                    .HasMaxLength(36)
                    .IsUnicode(false);

                //entity.Property(e => e.NumberOfAdults).HasColumnType("tinyint(1) unsigned");

                //entity.Property(e => e.NumberOfOtherOccupancies).HasColumnType("tinyint(1) unsigned");

                //entity.Property(e => e.NumberOfRooms).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ProfileEmails).IsUnicode(false);

                entity.Property(e => e.ProfileNames).IsUnicode(false);

                entity.Property(e => e.ProfilePhones).IsUnicode(false);

                entity.Property(e => e.ProfileReferenceIDs).IsUnicode(false);

                entity.Property(e => e.PromotionCode)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.PropertyCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.PropertyID)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.Property(e => e.PropertyName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RatePlanCodes).IsUnicode(false);

                entity.Property(e => e.RatePlanNames).IsUnicode(false);

                entity.Property(e => e.ReservationReferenceIDs).IsUnicode(false);

                entity.Property(e => e.RoomConfirmationNumber)
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeCodes).IsUnicode(false);

                entity.Property(e => e.RoomTypeNames).IsUnicode(false);

                //entity.Property(e => e.Status).HasColumnType("tinyint(1) unsigned");

                //entity.Property(e => e.Total).HasColumnType("decimal(13,4)");

                //entity.Property(e => e.TravelType).HasColumnType("tinyint(1) unsigned");

            });

            modelBuilder.Entity<ReservationCancellationInfo>(entity =>
            {
                entity.ToTable("ReservationCancellationInfo", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_CancellationInfo_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CancelPenaltyAmount).HasColumnType("decimal(13,4) unsigned");

                entity.Property(e => e.CancelPenaltyNumberNights).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.CancelPenaltyPercent).HasColumnType("decimal(3,2) unsigned");

                entity.Property(e => e.CancellationPolicyName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CancellationPolicyRefID).HasColumnType("char(36)");

                entity.Property(e => e.IsRefundable).HasColumnType("bit(1)");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.CancellationInfos)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CancellationInfo_Reservation");
            });

            modelBuilder.Entity<ReservationCancellationPolicy>(entity =>
            {
                entity.ToTable("ReservationCancellationPolicy", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_CancellationPolicy_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CanCancel).HasColumnType("bit(1)");

                entity.Property(e => e.CancelPenaltyAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.CancelPenaltyNumberNights).HasColumnType("tinyint(1)");

                entity.Property(e => e.CancelPenaltyPercent).HasColumnType("decimal(3,2)");

                entity.Property(e => e.CancellationDescription).IsUnicode(false);

                entity.Property(e => e.CancellationPolicyName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CancellationPolicyRefID).HasColumnType("char(36)");

                entity.Property(e => e.HourThresholdBeforeReservation).HasColumnType("int(10) unsigned");

                entity.Property(e => e.JsonValue).IsUnicode(false);

                entity.Property(e => e.NonRefundable).HasColumnType("bit(1)");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.CancellationPolicies)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CancellationPolicy_Reservation");
            });

            modelBuilder.Entity<ReservationExtra>(entity =>
            {
                entity.ToTable("ReservationExtra", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_Extra_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ExtraCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.JsonValue).IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnType("int(10) unsigned");

                entity.Property(e => e.RateAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.Type)
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Extras)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Reservation_Extra");
            });

            modelBuilder.Entity<ReservationExtraFee>(entity =>
            {
                entity.ToTable("ReservationExtraFee", "HmsOrient");

                entity.HasIndex(e => e.ReservationExtraID)
                    .HasName("FK_ExtraFee_Extra_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.FeeAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.FeeItem)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.FeeType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationExtraID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.ReservationExtra)
                    .WithMany(p => p.Fees)
                    .HasForeignKey(d => d.ReservationExtraID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExtraFee_Extra");
            });

            modelBuilder.Entity<ReservationExtraTax>(entity =>
            {
                entity.ToTable("ReservationExtraTax", "HmsOrient");

                entity.HasIndex(e => e.ReservationExtraID)
                    .HasName("FK_ExtraTax_Extra_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReservationExtraID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.TaxItem)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.TaxType).HasColumnType("tinyint(1) unsigned");

                entity.HasOne(d => d.ReservationExtra)
                    .WithMany(p => p.Taxes)
                    .HasForeignKey(d => d.ReservationExtraID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExtraTax_Extra");
            });

            modelBuilder.Entity<ReservationFee>(entity =>
            {
                entity.ToTable("ReservationFee", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_ReservationFee_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.FeeAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.FeeItem)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.FeeType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Fees)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReservationFee_Reservation");
            });

            modelBuilder.Entity<ReservationGuaranteeInfo>(entity =>
            {
                entity.ToTable("ReservationGuaranteeInfo", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_GuaranteeInfo_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.GuaranteeDescription).IsUnicode(false);

                entity.Property(e => e.GuaranteeMethod).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.GuaranteeName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.GuaranteeRefID).HasColumnType("char(36)");

                entity.Property(e => e.GuaranteeType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.GuaranteeValue).IsUnicode(false);

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.GuaranteeInfos)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GuaranteeInfo_Reservation");
            });

            modelBuilder.Entity<ReservationGuaranteePolicy>(entity =>
            {
                entity.ToTable("ReservationGuaranteePolicy", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_GuaranteePolicy_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DepositAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.DepositDays).HasColumnType("int(10) unsigned");

                entity.Property(e => e.DepositNumberNights).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.DepositPercent).HasColumnType("decimal(3,2)");

                entity.Property(e => e.DepositTiming).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.GuaranteePolicyDescription).IsUnicode(false);

                entity.Property(e => e.GuaranteePolicyName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.GuaranteePolicyRefID).HasColumnType("char(36)");

                entity.Property(e => e.JsonValue).IsUnicode(false);

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.GuaranteePolicies)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GuaranteePolicy_Reservation");
            });

            modelBuilder.Entity<ReservationLog>(entity =>
            {
                entity.ToTable("ReservationLog", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_Log_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Action).HasColumnType("tinyint(1)");

                entity.Property(e => e.Notes).IsUnicode(false);

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.UserRefID)
                    .IsRequired()
                    .HasColumnType("char(36)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Logs)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Log_Reservation");
            });

            modelBuilder.Entity<ReservationOtherOccupancy>(entity =>
            {
                entity.ToTable("ReservationOtherOccupancy", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_OtherOccupancy_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.OccupancyRefCode).HasColumnType("char(36)");

                entity.Property(e => e.OccupancyRefID).HasColumnType("char(36)");

                entity.Property(e => e.Quantity).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.OtherOccupancies)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OtherOccupancy_Reservation");
            });

            modelBuilder.Entity<ReservationProfile>(entity =>
            {
                entity.ToTable("ReservationProfile", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_Profile_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnType("longtext");

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                //entity.Property(e => e.GenderType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PassportID)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProfileRefID).HasColumnType("char(36)");

                //entity.Property(e => e.ProfileType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Profiles)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Profile_Reservation");
            });

            modelBuilder.Entity<ReservationProfileReferenceID>(entity =>
            {
                entity.ToTable("ReservationProfileReferenceID", "HmsOrient");

                entity.HasIndex(e => e.ReservationProfileID)
                    .HasName("FK_ProfileReference_Profile_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReferenceName)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceValue)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ReservationProfileID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.ReservationProfile)
                    .WithMany(p => p.ProfileReferenceIDs)
                    .HasForeignKey(d => d.ReservationProfileID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProfileReference_Profile");
            });

            modelBuilder.Entity<ReservationPropertyInfo>(entity =>
            {
                entity.ToTable("ReservationPropertyInfo", "HmsOrient");

                entity.HasIndex(e => e.ID)
                    .HasName("ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.ReservationID)
                    .HasName("fk_ReservationPropertyInfo_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.JsonValue)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithOne(p => p.PropertyInfo)
                    .HasForeignKey<ReservationPropertyInfo>(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ReservationPropertyInfo_Reservation");
            });

            modelBuilder.Entity<ReservationRatePlanInfo>(entity =>
            {
                entity.ToTable("ReservationRatePlanInfo", "HmsOrient");

                entity.HasIndex(e => e.ReservationRoomRateID)
                    .HasName("FK_RatePlanInfo_RoomRate_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.JsonValue).IsUnicode(false);

                entity.Property(e => e.RatePlanCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RatePlanDescription).IsUnicode(false);

                entity.Property(e => e.RatePlanName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ReservationRoomRateID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.ReservationRoomRate)
                    .WithOne(p => p.RatePlanInfo)
                    .HasForeignKey<ReservationRatePlanInfo>(d => d.ReservationRoomRateID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RatePlanInfo_RoomRate");
            });

            modelBuilder.Entity<ReservationReferenceID>(entity =>
            {
                entity.ToTable("ReservationReferenceID", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_ReferenceID_Reservation");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.Type).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.ReferenceIds)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReferenceID_Reservation");
            });

            modelBuilder.Entity<ReservationRequest>(entity =>
            {
                entity.ToTable("ReservationRequest", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_Request_Reservation");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.RequestContent)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.RequestType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Requests)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Request_Reservation");
            });

            modelBuilder.Entity<ReservationRoomRate>(entity =>
            {
                entity.ToTable("ReservationRoomRate", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_RoomRate_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.RateAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.RatePlanCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RatePlanName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RatePlanRefID).HasColumnType("char(36)");

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.RoomTypeCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeRefID).HasColumnType("char(36)");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.RoomRates)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoomRate_Reservation");
            });

            modelBuilder.Entity<ReservationRoomRateFee>(entity =>
            {
                entity.ToTable("ReservationRoomRateFee", "HmsOrient");

                entity.HasIndex(e => e.ReservationRoomRateID)
                    .HasName("FK_RoomRateFee_RoomRate");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.FeeAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.FeeItem)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.FeeType).HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.ReservationRoomRateID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.HasOne(d => d.ReservationRoomRate)
                    .WithMany(p => p.Fees)
                    .HasForeignKey(d => d.ReservationRoomRateID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoomRateFee_RoomRate");
            });

            modelBuilder.Entity<ReservationRoomRateTax>(entity =>
            {
                entity.ToTable("ReservationRoomRateTax", "HmsOrient");

                entity.HasIndex(e => e.ReservationRoomRateID)
                    .HasName("FK_RoomRateTax_RoomRate");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReservationRoomRateID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.TaxItem)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.TaxType).HasColumnType("tinyint(1) unsigned");

                entity.HasOne(d => d.ReservationRoomRate)
                    .WithMany(p => p.Taxes)
                    .HasForeignKey(d => d.ReservationRoomRateID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoomRateTax_RoomRate");
            });

            modelBuilder.Entity<ReservationRoomTypeAmenityInfo>(entity =>
            {
                entity.ToTable("ReservationRoomTypeAmenityInfo", "HmsOrient");

                entity.HasIndex(e => e.ReservationRoomTypeInfoID)
                    .HasName("fk_reservationRoomTypeInfo_reservationRoomTypeAmenityInfo_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RefID).HasColumnType("char(36)");

                entity.Property(e => e.ReservationRoomTypeInfoID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.Type).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.ReservationRoomTypeInfo)
                    .WithMany(p => p.ReservationRoomTypeAmenityInfo)
                    .HasForeignKey(d => d.ReservationRoomTypeInfoID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoomTypeAmenity_RoomTypeInfo");
            });

            modelBuilder.Entity<ReservationRoomTypeInfo>(entity =>
            {
                entity.ToTable("ReservationRoomTypeInfo", "HmsOrient");

                entity.HasIndex(e => e.ReservationRoomRateID)
                    .HasName("FK_RoomTypeInfo_RoomRate");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.JsonValue).IsUnicode(false);

                entity.Property(e => e.ReservationRoomRateID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.RoomTypeCode)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.RoomTypeDescription).IsUnicode(false);

                entity.Property(e => e.RoomTypeName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.HasOne(d => d.ReservationRoomRate)
                    .WithOne(p => p.RoomTypeInfo)
                    .HasForeignKey<ReservationRoomTypeInfo>(d => d.ReservationRoomRateID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoomTypeInfo_RoomRate");
            });

            modelBuilder.Entity<ReservationTax>(entity =>
            {
                entity.ToTable("ReservationTax", "HmsOrient");

                entity.HasIndex(e => e.ReservationID)
                    .HasName("FK_Tax_Reservation_idx");

                entity.Property(e => e.ID)
                    .HasColumnType("binary(16)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ReservationID)
                    .IsRequired()
                    .HasColumnType("binary(16)");

                entity.Property(e => e.TaxAmount).HasColumnType("decimal(13,4)");

                entity.Property(e => e.TaxItem)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.TaxType).HasColumnType("tinyint(1) unsigned");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.Taxes)
                    .HasForeignKey(d => d.ReservationID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tax_Reservation");
            });
        }
    }
}
