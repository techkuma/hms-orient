﻿using System;
using Booking.Domain.Models;
using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class ProfileDto
    {
        [JsonProperty("profileRefID")]
        public Guid ProfileRefID { get; set; }

        [JsonProperty("email")]
        public string EmailAddress { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("middleName")]
        public string MiddleName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("profileType")]
        public ProfileType ProfileType { get; set; }

        [JsonProperty("passport")]
        public PassportDto Passport { get; set; }

        [JsonProperty("gender")]
        public GenderType GenderType { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
