﻿using System.Collections.Generic;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class RoomOccupancyDto
    {
        [JsonProperty("numberOfAdult")]
        public long NumberOfAdult { get; set; }

        [JsonProperty("otherOccupancies")]
        public List<OtherOccupancyDto> OtherOccupancies { get; set; }
    }
}
