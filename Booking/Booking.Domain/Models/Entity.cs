using System;

namespace Booking.Domain.Models
{
    public abstract class Entity 
    {
        public Guid ID { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}