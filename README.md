HMS Demo
-----

ASP.Net Core + Mysql + Docker demonstration
## Prerequisites
This demo require Docker to be installed. Please install Docker at https://docs.docker.com/install/

## Commands
To start demo, go to project root folder and run:
```shell
docker-compose up -d booking.mysql && dotnet run -p Booking/Booking.API/
```

## Testing APIs
There are 5 APIs. They can be viewed and tested on Swagger document UI at http://localhost:5000  when demo started.
### 1. Single creation:

Create one reservation

Endpoint: POST /api/v1/Booking

Sample data: located at Samples/single_import.json

### 2. Multi-creation

Create more than one reservation

Endpoint: POST /api/v1/Booking/bulkinsert

Sample data: located at Samples/2_records_import.json

### 3. Import JSON data file

Mass import of reservations by uploading JSON file

Endpoint: POST /api/v1/Booking/import

Sample data: located at Samples/hulk_import.json

### 4. Get one reservation

Endpoint: GET /api/v1/Booking/{id}

Example: /api/v1/Booking/1c2dfb84-ca38-4cbd-bc34-e8d34641e03c

### 5. Get multiple reservations

Endpoint: GET /api/v1/Booking/get-by-ids?ids={id1}&ids={id2}...&ids={idN}

Example: /api/v1/Booking/get-by-ids?ids=ab21ba8d-2727-4617-800a-82152412a59d&ids=d729f708-e508-4453-a0d8-7a3e0a1e67a0