﻿using System;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class ThumbnailDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
