﻿namespace Booking.Domain.Models
{
    public enum TaxType
    {
        NotSpecific = 0,
        Inclusive = 1,
        Exclusive = 2
    }
}
