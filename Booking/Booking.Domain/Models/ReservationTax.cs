﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationTax : TaxBase
    {
        public Guid ReservationID { get; set; }
        public virtual Reservation Reservation { get; set; }
    }
}
