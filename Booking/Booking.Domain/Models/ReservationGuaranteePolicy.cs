﻿using System;
using System.Collections.Generic;

namespace Booking.Domain.Models
{
    public partial class ReservationGuaranteePolicy : Entity
    {
        public Guid ReservationID { get; set; }
        public string GuaranteePolicyRefID { get; set; }
        public string GuaranteePolicyName { get; set; }
        public string GuaranteePolicyDescription { get; set; }
        public string JsonValue { get; set; }
        public decimal? DepositAmount { get; set; }
        public decimal? DepositPercent { get; set; }
        public int? DepositNumberNights { get; set; }
        public int? DepositTiming { get; set; }
        public int? DepositDays { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
