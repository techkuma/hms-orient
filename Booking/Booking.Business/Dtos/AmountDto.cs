﻿
using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class AmountDto
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }
    }
}
