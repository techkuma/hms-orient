﻿using AutoMapper;
using Booking.API.Infrastructure.Queues;
using Booking.Business.Dtos;
using Booking.Business.Services;
using Booking.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Booking.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBookingService _bookingService;
        private readonly IMapper _mapper;
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;

        public BookingController(IBookingService bookingService, IMapper mapper, IBackgroundTaskQueue backgroundTaskQueue)
        {
            _bookingService = bookingService;
            _mapper = mapper;
            _backgroundTaskQueue = backgroundTaskQueue;
        }

        /// <summary>
        /// Create Single Reservation (Implemented by Dinh Luu)
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Reservation>> Create([FromBody]ReservationDto reservation)
        {
            var reservationBase = _mapper.Map<Reservation>(reservation);
            return Ok(await _bookingService.Create(reservationBase));
        }

        /// <summary>
        /// Create multiple Reservations (Implemented by Dinh Luu)
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        [HttpPost("bulkinsert")]
        public async Task<ActionResult<IEnumerable<Reservation>>> BulkCreate([FromBody]IEnumerable<ReservationDto> reservations)
        {
            var reservationBases = _mapper.Map<IList<Reservation>>(reservations);
            return Ok(await _bookingService.Create(reservationBases));
        }

        /// <summary>
        /// Imnport multiple Reservations from file (Implemented by Hung Phan)
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        [HttpPost("import")]
        [DisableRequestSizeLimit] // For demonstration only
        public async Task<ActionResult> Import(IFormFile uploadedJsonFile)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            if (uploadedJsonFile == null || uploadedJsonFile.Length == 0)
            {
                throw new ArgumentNullException("File is not selected");
            }

            // Save uploaded file
            var uploadFolder = Path.Combine("wwwroot", "uploads");
            var generatedFileName = DateTime.UtcNow.ToString("yyyyMMdd-HHmmss-") + uploadedJsonFile.FileName;
            var savePath = Path.Combine(Directory.GetCurrentDirectory(), uploadFolder, generatedFileName);

            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                await uploadedJsonFile.CopyToAsync(stream);
            }

            // Queue importing
            _backgroundTaskQueue.QueueBackgroundWorkItem(async token =>
            {
                FileStream fs = new FileStream(savePath, FileMode.Open, FileAccess.Read);

                await _bookingService.ImportFromStream(fs);
            });

            stopwatch.Stop();

            return Ok(new
            {
                title = $@"Successfully save import file and queue importing in {Math.Round((decimal)stopwatch.ElapsedMilliseconds / 1000, 2)}s "
            });
        }

        /// <summary>
        /// Get single Reservation (Implemented by Dat)
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Reservation), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<Reservation>>> GetReservationsAsync(Guid id)
        {
            var dataResults = await _bookingService.GetRecordByID(id);
            var reservationBases = _mapper.Map<ReservationDto>(dataResults);
            return Ok(reservationBases);

        }

        /// <summary>
        /// Get multiple Reservations (Implemented by Dat)
        /// </summary>
        /// <param name="reservation"></param>
        /// <returns></returns>
        [HttpGet("get-by-ids")]
        [ProducesResponseType(typeof(IEnumerable<Reservation>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<Reservation>>> GetReservationsAsync([FromQuery]IEnumerable<Guid> ids)
        {
            var dataResult = await _bookingService.GetRecordByID(ids);
            var reservationBases = _mapper.Map<IEnumerable<ReservationDto>>(dataResult);
            return Ok(reservationBases);
        }
    }
}
