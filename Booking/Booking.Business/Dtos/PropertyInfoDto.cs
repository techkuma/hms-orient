﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class PropertyInfoDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("countryID")]
        public Guid CountryID { get; set; }

        [JsonProperty("countryStateId")]
        public Guid CountryStateID { get; set; }

        [JsonProperty("hotelTypeId")]
        public Guid HotelTypeID { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("telephone")]
        public object Telephone { get; set; }

        [JsonProperty("fax")]
        public string Fax { get; set; }

        [JsonProperty("webAddress")]
        public string WebAddress { get; set; }

        [JsonProperty("currencySymbol")]
        public string CurrencySymbol { get; set; }

        [JsonProperty("locationCoordinates")]
        public string LocationCoordinates { get; set; }

        [JsonProperty("star")]
        public long Star { get; set; }

        [JsonProperty("country")]
        public CountryDto Country { get; set; }

        [JsonProperty("state")]
        public CountryDto State { get; set; }

        [JsonProperty("hotelType")]
        public HotelTypeDto HotelType { get; set; }

        [JsonProperty("checkInTime")]
        public DateTime CheckInTime { get; set; }

        [JsonProperty("checkOutTime")]
        public DateTime CheckOutTime { get; set; }

        [JsonProperty("tripAdvisor")]
        public object TripAdvisor { get; set; }

        [JsonProperty("thumbnails")]
        public List<ThumbnailDto> Thumbnails { get; set; }

        [JsonProperty("extends")]
        public List<HotelTypeDto> Extends { get; set; }

        [JsonProperty("roomTypes")]
        public object RoomTypes { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedTime { get; set; } = DateTime.Now;

        [JsonProperty("updatedAt")]
        public DateTime UpdatedTime { get; set; } = DateTime.Now;
    }
}
