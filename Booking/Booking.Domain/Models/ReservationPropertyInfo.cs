﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationPropertyInfo : Entity
    {
        public Guid ReservationID { get; set; }
        public string JsonValue { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
