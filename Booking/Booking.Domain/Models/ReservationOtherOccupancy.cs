﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationOtherOccupancy : Entity
    {
        public Guid ReservationID { get; set; }
        public string OccupancyRefID { get; set; }
        public string OccupancyRefCode { get; set; }
        public int Quantity { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
