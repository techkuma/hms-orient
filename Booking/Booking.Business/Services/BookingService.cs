﻿using Booking.Domain.Models;
using Booking.Infrastructure.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Booking.Infrastructure.Context;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading.Tasks.Dataflow;
using Newtonsoft.Json;
using Booking.Business.Dtos;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace Booking.Business.Services
{
    public interface IBookingService
    {
        Task<bool> Create(Reservation newRecord);
        Task<bool> Create(IEnumerable<Reservation> newRecords);
        Task<Reservation> GetRecordByID(Guid id);
        Task<IEnumerable<Reservation>> GetRecordByID(IEnumerable<Guid> ids);
        Task ImportFromStream(Stream fileStream);
    }

    public class BookingService : IBookingService
    {
        private readonly IRepository<Reservation> _reservationRepository;
        private readonly string _connectionString;
        private readonly int _importChunkSize;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        public BookingService(IRepository<Reservation> reservationRepository, IConfiguration configuration, IMapper mapper, ILogger<BookingService> logger)
        {
            _reservationRepository = reservationRepository;
            _configuration = configuration;
            _connectionString = $@"Server={configuration["MYSQL_SERVER_NAME"]}; Port={configuration["MYSQL_SERVER_PORT"]}; Database={configuration["MYSQL_DATABASE"]}; Uid={configuration["MYSQL_USER"]}; Pwd={configuration["MYSQL_PASSWORD"]}";
            _importChunkSize = Convert.ToInt32(configuration["IMPORT_CHUNK_SIZE"] ?? "100");
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<bool> Create(Reservation newRecord)
        {
            try
            {
                await _reservationRepository.Create(newRecord, true);
                return true;
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                _logger.LogError(ex.Message, ex);

                return false;
            }
        }

        public async Task<bool> Create(IEnumerable<Reservation> newRecords)
        {
            int chunkSize = _configuration.GetValue<int>("IMPORT_CHUNK_SIZE");
            try
            {
                List<Task> tasks = new List<Task>();
                BookingContext context = CreateNonTrackingContext();
                int count = 0;

                foreach (var record in newRecords)
                {
                    context.Set<Reservation>().Add(record);

                    if (++count % chunkSize == 0)
                    {
                        Task chunkSaveTask = ChunkSave(context);
                        tasks.Add(chunkSaveTask);
                        context = CreateNonTrackingContext();
                    }
                }

                // save rest items
                if (count % chunkSize != 0)
                {
                    Task chunkSaveTask = ChunkSave(context);
                    tasks.Add(chunkSaveTask);
                }

                await Task.WhenAll(tasks);
                return true;
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                _logger.LogError(ex.Message, ex);

                return false;
            }
        }

        async Task ChunkSave(BookingContext context)
        {
            await context.SaveChangesAsync();
            context.Dispose();
        }

        public async Task<Reservation> GetRecordByID(Guid id)
        {
            var reservation =  _reservationRepository.FindByCondition(r =>r.ID == id)
                .Include(r =>r.OtherOccupancies)
                .Include(r => r.PropertyInfo)
                .Include(r => r.Profiles)
                .FirstOrDefaultAsync();

            return await reservation;
        }

        public async Task<IEnumerable<Reservation>> GetRecordByID(IEnumerable<Guid> ids)
        {
            var reservations = await _reservationRepository.FindByCondition(r => ids.Contains(r.ID))
                .Include(r => r.OtherOccupancies)
                .Include(r => r.PropertyInfo)
                .Include(r => r.Profiles)
                .AsNoTracking()
                .ToListAsync();

            return reservations;
        }

        BookingContext CreateNonTrackingContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<BookingContext>();
            optionsBuilder.UseMySql(_connectionString);

            var context = new BookingContext(optionsBuilder.Options);
            context.ChangeTracker.AutoDetectChangesEnabled = false;

            return context;
        }

        public Task ImportFromStream(Stream fileStream)
        {
            var saveBatch = new BatchBlock<Reservation>(_importChunkSize);
            var saveAction = new ActionBlock<IEnumerable<Reservation>>(async (IEnumerable<Reservation> reservations) =>
            {
                try
                {
                    BookingContext localContext = CreateNonTrackingContext();
                    var reservationRepository = localContext.Set<Reservation>();

                    foreach (Reservation reservation in reservations)
                    {
                        await reservationRepository.AddAsync(reservation);
                    }

                    await localContext.SaveChangesAsync();
                    localContext.Dispose();
                    System.GC.Collect(); // TODO: Free memory in periods
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    _logger.LogError(ex.Message, ex);
                }
            }, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = Environment.ProcessorCount });

            saveBatch.LinkTo(saveAction);

            JsonSerializer serializer = new JsonSerializer();
            using (StreamReader streamReader = new StreamReader(fileStream))
            using (JsonReader reader = new JsonTextReader(streamReader))
            {
                ReservationDto reservation;
                while (reader.Read())
                {
                    // deserialize only when there's "{" character in the stream
                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        reservation = serializer.Deserialize<ReservationDto>(reader);

                        saveBatch.Post(_mapper.Map<Reservation>(reservation));
                    }
                }
            }

            saveBatch.Completion.ContinueWith(delegate {
                saveAction.Complete();
            });
            saveBatch.Complete();

            return saveAction.Completion;
        }
    }
}
