﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationRoomRateFee : FeeBase
    {
        public Guid ReservationRoomRateID { get; set; }
        public virtual ReservationRoomRate ReservationRoomRate { get; set; }
    }
}
