﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationRoomRateTax : TaxBase
    {
        public Guid ReservationRoomRateID { get; set; }
        public virtual ReservationRoomRate ReservationRoomRate { get; set; }
    }
}
