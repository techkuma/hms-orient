﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Booking.API.Infrastructure.Queues;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Booking.API.Infrastructure.QueuedServices
{
    public class QueuedImportService : BackgroundService
    {
        private readonly IBackgroundTaskQueue _taskQueue;
        private readonly ILogger<QueuedImportService> _logger;

        public QueuedImportService (IBackgroundTaskQueue taskQueue, ILogger<QueuedImportService> logger)
        {
            _taskQueue = taskQueue;
            _logger = logger;
        }

        protected async override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var workItem = await _taskQueue.DequeueAsync(cancellationToken);

                    await workItem(cancellationToken);
                } catch (Exception ex)
                {
                    _logger.LogError(ex.Message, ex);
                }
            }
        }
    }
}
