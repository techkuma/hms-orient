﻿using System;
using System.Collections.Generic;
using Booking.Domain.Models;
using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class ReservationDto
    {
        [JsonProperty("reservationID")]
        public Guid ID { get; set; }

        [JsonProperty("status")]
        public ReservationStatus Status { get; set; }

        [JsonProperty("propertyID")]
        public Guid PropertyID { get; set; }

        [JsonProperty("propertyCode")]
        public string PropertyCode { get; set; }

        [JsonProperty("propertyName")]
        public string PropertyName { get; set; }

        [JsonProperty("arrivalDate")]
        public DateTime ArrivalDate { get; set; }

        [JsonProperty("departureDate")]
        public DateTime DepartureDate { get; set; }

        [JsonProperty("bookingDate")]
        public DateTime BookingDate { get; set; }

        [JsonProperty("promotionCode")]
        public string PromotionCode { get; set; }

        [JsonProperty("confirmationNumber")]
        public string RoomConfirmationNumber { get; set; }

        [JsonProperty("itineraryNumber")]
        public string ItineraryNumber { get; set; }

        [JsonProperty("roomOccupancy")]
        public RoomOccupancyDto RoomOccupancy { get; set; }

        [JsonProperty("roomRates")]
        public List<RoomRateDto> RoomRates { get; set; }

        [JsonProperty("packages")]
        public List<PackageDto> Packages { get; set; }

        [JsonProperty("profiles")]
        public List<ProfileDto> Profiles { get; set; }

        [JsonProperty("guaranteeInfos")]
        public List<GuaranteeInfoDto> GuaranteeInfos { get; set; }

        [JsonProperty("cancelInfos")]
        public object CancelInfos { get; set; }

        [JsonProperty("total")]
        public TotalDto Total { get; set; }

        [JsonProperty("specialRequests")]
        public object SpecialRequests { get; set; }

        [JsonProperty("referenceIds")]
        public object ReferenceIDs { get; set; }

        [JsonProperty("cancellationPolicies")]
        public object CancellationPolicies { get; set; }

        [JsonProperty("propertyInfo")]
        public PropertyInfoDto PropertyInfo { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
