﻿namespace Booking.Domain.Models
{
    public enum GuaranteeType
    {
        NotSpecific = 0,
        None = 1,
        GuaranteeAccepted = 2,
        GuaranteeRequired = 3
    }
}
