﻿using System;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class RoomRateDto
    {
        [JsonProperty("stayDate")]
        public DateTime StayDate { get; set; }

        [JsonProperty("roomTypeCode")]
        public string RoomTypeCode { get; set; }

        [JsonProperty("roomTypeName")]
        public string RoomTypeName { get; set; }

        [JsonProperty("roomTypeRefID")]
        public Guid RoomTypeRefID { get; set; }

        [JsonProperty("ratePlanCode")]
        public string RatePlanCode { get; set; }

        [JsonProperty("ratePlanName")]
        public string RatePlanName { get; set; }

        [JsonProperty("ratePlanRefID")]
        public Guid RatePlanRefID { get; set; }

        [JsonProperty("roomTypeInfo")]
        public RoomTypeInfoDto RoomTypeInfo { get; set; }

        [JsonProperty("ratePlanInfo")]
        public RatePlanInfoDto RatePlanInfo { get; set; }

        [JsonProperty("rateAmount")]
        public TotalDto RateAmount { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
