﻿using System;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class CountryDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
