﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Business.Dtos
{
    public class CancellationInfosDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("reservationID")]
        public Guid ReservationID { get; set; }

        [JsonProperty("cancellationPolicyRefID")]
        public string CancellationPolicyRefID { get; set; }

        [JsonProperty("cancellationPolicyRefID")]
        public string CancellationPolicyName { get; set; }

        [JsonProperty("isRefundable")]
        public bool IsRefundable { get; set; }

        [JsonProperty("cancelPenaltyAmount")]
        public decimal? CancelPenaltyAmount { get; set; }

        [JsonProperty("cancelPenaltyPercent")]
        public decimal? CancelPenaltyPercent { get; set; }

        [JsonProperty("cancelPenaltyNumberNights")]
        public int CancelPenaltyNumberNights { get; set; }

        [JsonProperty("createdTime")]
        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
