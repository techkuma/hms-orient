﻿using Autofac;
using Booking.Infrastructure.Context;
using Booking.Business.Services;
using Booking.Infrastructure.Contracts;
using Booking.API.Infrastructure.Queues;

namespace Booking.API
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterType<BookingService>().As<IBookingService>().InstancePerLifetimeScope();
        }
    }
}
