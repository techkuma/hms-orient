﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationRatePlanInfo : Entity
    {
        public Guid ReservationRoomRateID { get; set; }
        public string RatePlanName { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanDescription { get; set; }
        public string JsonValue { get; set; }

        public virtual ReservationRoomRate ReservationRoomRate { get; set; }
    }
}
