﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class RoomTypeInfoDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("name")]
        public string RoomTypeName { get; set; }

        [JsonProperty("code")]
        public string RoomTypeCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("shortDescription")]
        public string ShortDescription { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("numberOfRoom")]
        public long NumberOfRoom { get; set; }

        [JsonProperty("defaultOccupancy")]
        public long DefaultOccupancy { get; set; }

        [JsonProperty("maxOccupancy")]
        public long MaxOccupancy { get; set; }

        [JsonProperty("maxAdult")]
        public long MaxAdult { get; set; }

        [JsonProperty("maxChild")]
        public long MaxChild { get; set; }

        [JsonProperty("hotelID")]
        public Guid HotelID { get; set; }

        [JsonProperty("activeDate")]
        public DateTime ActiveDate { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("squareUnit")]
        public long SquareUnit { get; set; }

        [JsonProperty("thumbnails")]
        public List<ThumbnailDto> Thumbnails { get; set; }

        [JsonProperty("extends")]
        public List<HotelTypeDto> Extends { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
