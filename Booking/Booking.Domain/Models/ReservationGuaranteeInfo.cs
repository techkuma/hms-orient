﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationGuaranteeInfo : Entity
    {
        public Guid ReservationID { get; set; }
        public string GuaranteeRefID { get; set; }
        public string GuaranteeName { get; set; }
        public GuaranteeType GuaranteeType { get; set; }
        public GuaranteeMethod GuaranteeMethod { get; set; }
        public string GuaranteeValue { get; set; }
        public string GuaranteeDescription { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
