﻿using Booking.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Booking.Domain.Models;
using System.Threading.Tasks;

namespace Booking.Infrastructure.Contracts
{
    public interface IRepository<T>
    {
        Task<T> Get(params object[] key);
        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        Task Create(T entity, bool isSaved = false);
        Task Update(T entity, bool isSaved = false);
        Task Delete(T entity, bool isSaved = false);
        Task Save();
    }

    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected BookingContext Context { get; set; }

        public Repository(BookingContext RepositoryContext)
        {
            Context = RepositoryContext;
        }

        public async Task<T> Get(params object[] key)
        {
            return await Context.FindAsync<T>(key);
        }

        public IQueryable<T> FindAll()
        {
            return Context.Set<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>().Where(expression);
        }

        public async Task Create(T entity, bool isSaved = false)
        {
            Context.Set<T>().Add(entity);
            if (isSaved)
            {
                await Context.SaveChangesAsync();
            }
        }

        public async Task Update(T entity, bool isSaved = false)
        {
            Context.Set<T>().Update(entity);
            if (isSaved)
            {
                await Context.SaveChangesAsync();
            }
        }

        public async Task Delete(T entity, bool isSaved =  false)
        {
            Context.Set<T>().Remove(entity);
            if (isSaved)
            {
                await Context.SaveChangesAsync();
            }
        }

        public async Task Save()
        {
            try
            {
                await Context.SaveChangesAsync();
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
