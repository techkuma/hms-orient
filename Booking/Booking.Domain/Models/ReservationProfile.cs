﻿using System;
using System.Collections.Generic;

namespace Booking.Domain.Models
{
    public class ReservationProfile: Entity
    {
        public Guid ReservationID { get; set; }
        public Guid ProfileRefID { get; set; }
        public ProfileType ProfileType { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PassportID { get; set; }
        public DateTime PassportExpiredDate { get; set; }
        public GenderType GenderType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }

        public virtual Reservation Reservation { get; set; }
        public virtual IList<ReservationProfileReferenceID> ProfileReferenceIDs { get; set; }
    }
}
