﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Autofac;

using Booking.Infrastructure.Context;
using Booking.API.Infrastructure.Queues;
using Booking.API.Infrastructure.Extensions;
using Booking.API.Infrastructure.QueuedServices;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http.Features;

namespace Booking.API
{
    public class Startup
    {
        private readonly string _connectionString;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _connectionString = $@"Server={configuration["MYSQL_SERVER_NAME"]}; Port={configuration["MYSQL_SERVER_PORT"]}; Database={configuration["MYSQL_DATABASE"]}; Uid={configuration["MYSQL_USER"]}; Pwd={configuration["MYSQL_PASSWORD"]}";
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BookingContext>(ops => ops.UseMySql(_connectionString));
            services.AddAutoMapperWithConfig();
            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            services.AddHostedService<QueuedImportService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.Configure<FormOptions>(x =>
            {
                // For demonstration only
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });

            ConfigureSwagger(services);
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "HmsOrient Booking API",
                    Description = "HmsOrient Booking API (ASP.NET Core)",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "OrientSoftware Admin", Email = "admin@orientsoftware.net", Url = "" },
                });

                c.DescribeAllParametersInCamelCase();
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseHandleErrorsInResponse();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            //app.UseHandleErrorsInResponse();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "HmsOrient Booking API V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseMvc();
        }
    }
}
