﻿using System;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class PackageDto
    {
        [JsonProperty("usedDate")]
        public DateTime UsedDate { get; set; }

        [JsonProperty("packageRefID")]
        public object PackageRefID { get; set; }

        [JsonProperty("packageCode")]
        public Guid PackageCode { get; set; }

        [JsonProperty("packageName")]
        public Guid PackageName { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("amount")]
        public TotalDto Amount { get; set; }
    }
}
