﻿using System;

namespace Booking.Domain.Models
{
    public class FeeBase : Entity
    {
        public string FeeItem { get; set; }
        public FeeType FeeType { get; set; }
        public decimal FeeAmount { get; set; }
    }
}
