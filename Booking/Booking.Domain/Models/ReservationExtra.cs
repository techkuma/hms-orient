﻿using System;
using System.Collections.Generic;

namespace Booking.Domain.Models
{
    public class ReservationExtra : Entity
    {
        public Guid ReservationID { get; set; }
        public DateTime StayDate { get; set; }
        public string ExtraCode { get; set; }
        public string ExtraName { get; set; }
        public ExtraType Type { get; set; }
        public decimal RateAmount { get; set; }
        public int Quantity { get; set; }
        public string JsonValue { get; set; }

        public virtual Reservation Reservation { get; set; }
        public virtual IList<ReservationExtraFee> Fees { get; set; }
        public virtual IList<ReservationExtraTax> Taxes { get; set; }
    }
}
