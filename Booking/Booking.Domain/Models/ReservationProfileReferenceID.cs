﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationProfileReferenceID : Entity
    {
        public Guid ReservationProfileID { get; set; }
        public string ReferenceName { get; set; }
        public string ReferenceValue { get; set; }

        public virtual ReservationProfile ReservationProfile { get; set; }
    }
}
