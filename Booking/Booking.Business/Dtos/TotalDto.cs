﻿using System.Collections.Generic;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class TotalDto
    {
        [JsonProperty("amount")]
        public AmountDto Amount { get; set; }

        [JsonProperty("taxFeeItems")]
        public List<object> TaxFeeItems { get; set; }
    }
}
