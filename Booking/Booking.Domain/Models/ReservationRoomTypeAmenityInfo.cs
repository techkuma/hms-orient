﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationRoomTypeAmenityInfo : Entity
    {
        public Guid ReservationRoomTypeInfoID { get; set; }
        public Guid RefID { get; set; }
        public AmenityType Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ReservationRoomTypeInfo ReservationRoomTypeInfo { get; set; }
    }
}
