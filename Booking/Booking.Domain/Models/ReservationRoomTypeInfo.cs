﻿using System;
using System.Collections.Generic;

namespace Booking.Domain.Models
{
    public class ReservationRoomTypeInfo : Entity
    {
        public Guid ReservationRoomRateID { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeCode { get; set; }
        public string RoomTypeDescription { get; set; }
        public string JsonValue { get; set; }

        public virtual ReservationRoomRate ReservationRoomRate { get; set; }

        public IList<ReservationRoomTypeAmenityInfo> Amenities { get; set; }
        public IList<ReservationRoomTypeAmenityInfo> ReservationRoomTypeAmenityInfo { get; set; }
    }
}
