﻿using System;

namespace Booking.Domain.Models
{
    public class TaxBase : Entity
    {
        public string TaxItem { get; set; }
        public TaxType TaxType { get; set; }
        public decimal TaxAmount { get; set; }
    }
}
