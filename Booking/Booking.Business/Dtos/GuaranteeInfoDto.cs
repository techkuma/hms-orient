﻿using System;

using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public partial class GuaranteeInfoDto
    {
        [JsonProperty("guaranteeRefID")]
        public Guid GuaranteeRefID { get; set; }

        [JsonProperty("guaranteeName")]
        public Guid GuaranteeName { get; set; }

        [JsonProperty("guaranteeType")]
        public string GuaranteeType { get; set; }

        [JsonProperty("guaranteeMethod")]
        public string GuaranteeMethod { get; set; }

        [JsonProperty("guaranteeValue")]
        public Guid GuaranteeValue { get; set; }

        [JsonProperty("guaranteeDescription")]
        public Guid GuaranteeDescription { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
