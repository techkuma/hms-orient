﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationExtraTax : TaxBase
    {
        public virtual ReservationExtra ReservationExtra { get; set; }
        public Guid ReservationExtraID { get; set; }
    }
}
