# /bin/bash

MODELS_PATH="./Models"
CONTEXT_PATH="../Booking.Infrastructure/Context"
CURRENT_NAMESPACE="Booking.Domain.Models"
CONTEXT_NAMESPACE="Booking.Infrastructure.Context"

# Load configuration
ENV_FILE=../../.env
GetVariable () {
    echo $(grep $1 $ENV_FILE | cut -d '=' -f 2-)
}

MYSQL_DATABASE="$(GetVariable MYSQL_DATABASE)"
MYSQL_USER="$(GetVariable MYSQL_USER)"
MYSQL_PASSWORD="$(GetVariable MYSQL_PASSWORD)"

generate () {
    # Reset results
    echo "Resetting old files..."
    find ${MODELS_PATH} -type f ! -name "Entity.cs" | xargs grep -iL "enum " | xargs rm -f
    cp "./scripts/BookingContext.cs.template" "$CONTEXT_PATH/BookingContext.cs"

    # Generate models and context
    echo "Generating models and context"
    dotnet ef dbcontext scaffold "server=localhost;port=33606;user=$MYSQL_USER;password=$MYSQL_PASSWORD;database=$MYSQL_DATABASE" MySql.Data.EntityFrameworkCore -o "$MODELS_PATH" -f --use-database-names

    echo "Inheriting all from Entity class"
    # Remove field: Id
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -E "s/public byte\[\] ID \{ get; set; \}//g" {} \; 1>/dev/null
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -E "s/public Guid ID \{ get; set; \}//g" {} \; 1>/dev/null
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -E "s/public int\[\] ID \{ get; set; \}//g" {} \; 1>/dev/null
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -E "s/public string ID \{ get; set; \}//g" {} \; 1>/dev/null
    # Add Entity as base class
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' ! -name '*Context.cs' -exec sed -i -E "s/class (.+)/class \1: Entity/g" {} \; 1>/dev/null

    echo "Moving context to infrastructure"
    mv "${MODELS_PATH}/${MYSQL_DATABASE}Context.cs" "$CONTEXT_PATH/BookingContext.cs"
    # Replace namespace
    sed -i -e "s/$CURRENT_NAMESPACE/$CONTEXT_NAMESPACE/g" "$CONTEXT_PATH/BookingContext.cs"
    # Replace class name
    sed -i -e "s/${MYSQL_DATABASE}Context/BookingContext/g" "$CONTEXT_PATH/BookingContext.cs"
    # Add using for models
    sed -i "5i using ${CURRENT_NAMESPACE};" "$CONTEXT_PATH/BookingContext.cs"
    # Remove unused lines
    sed -i "43,50d" "$CONTEXT_PATH/BookingContext.cs"
    sed -i "10,12d" "$CONTEXT_PATH/BookingContext.cs"
}

fixtypes() {
    echo "Injecting enum types into models"
    ENUM_SEARCH_STRING=$(grep -rlnw ./Models -e 'enum' | sed -e 's/.\/Models\///g' | sed -e 's/.cs//g' | sed '/^[[:space:]]*$/d' | tr '\n' '|')
    ENUM_SEARCH="(${ENUM_SEARCH_STRING%?})"
    find "$MODELS_PATH" -type f -exec sed -i -E "s/byte $ENUM_SEARCH/\1 \1/g" {} \; 1>/dev/null
	find "$MODELS_PATH" -type f -exec sed -i -E "s/int $ENUM_SEARCH/\1 \1/g" {} \; 1>/dev/null
	find "$MODELS_PATH" -type f -exec sed -i -E "s/int\? $ENUM_SEARCH/\1? \1/g" {} \; 1>/dev/null

    echo "Fixing data types"
    # Convert byte to int
    find "$MODELS_PATH" -type f -exec sed -i -E "s/byte/int/g" {} \; 1>/dev/null
    # Convert short (bit) to bool
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -e "s/ short? / bool /g" {} \; 1>/dev/null
    # Convert int[] to Guid
    find "$MODELS_PATH" -type f ! -name 'Entity.cs' -exec sed -i -e "s/ int\[\] / Guid /g" {} \; 1>/dev/null
}

MODE="$1"

if [ "$MODE" == "" ]; then
   generate
   fixtypes
elif [ "$MODE" == "generate" ]; then
   echo "Mode is $MODE"

   generate
elif [ "$MODE" == "fixtypes" ]; then
    echo "Mode is $MODE"

    fixtypes
else
   echo "Unknown mode"
fi