﻿namespace Booking.Domain.Models
{
    public enum TravelType
    {
        NotSpecific = 0,
        Leisure = 1,
        Business = 2
    }
}
