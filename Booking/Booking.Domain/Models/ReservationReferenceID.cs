﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationReferenceID : Entity
    {
        public Guid ReservationID { get; set; }
        public ReferenceType Type { get; set; }
        public string Value { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
