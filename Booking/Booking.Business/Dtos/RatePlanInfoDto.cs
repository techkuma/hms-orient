﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.Business.Dtos
{
    public class RatePlanInfoDto
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonProperty("roomRateID")]
        public Guid ReservationRoomRateID { get; set; }

        [JsonProperty("ratePlanName")]
        public string RatePlanName { get; set; }

        [JsonProperty("ratePlanCode")]
        public string RatePlanCode { get; set; }

        [JsonProperty("ratePlanDescription")]
        public string RatePlanDescription { get; set; }

        [JsonProperty("jsonValue")]
        public string JsonValue { get; set; }

        public DateTime CreatedTime { get; set; } = DateTime.Now;
    }
}
