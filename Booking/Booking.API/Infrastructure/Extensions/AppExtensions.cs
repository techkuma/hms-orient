﻿using Booking.API.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Booking.API.Infrastructure.Extensions
{
    public static class AppExtensions
    {
        public static IApplicationBuilder UseHandleErrorsInResponse(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorResponseMiddleware>();
        }
    }
}
