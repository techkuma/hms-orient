﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationExtraFee : FeeBase
    {
        public Guid ReservationExtraID { get; set; }
        public virtual ReservationExtra ReservationExtra { get; set; }
    }
}
