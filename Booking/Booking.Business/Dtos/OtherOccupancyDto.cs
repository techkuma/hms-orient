﻿
using Newtonsoft.Json;
namespace Booking.Business.Dtos
{
    public class OtherOccupancyDto
    {
        [JsonProperty("otherOccupancyRefID")]
        public string OtherOccupancyRefID { get; set; }

        [JsonProperty("otherOccupancyRefCode")]
        public string OtherOccupancyRefCode { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }
    }
}
