﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationFee : FeeBase
    {
        public virtual Reservation Reservation { get; set; }
        public Guid ReservationID { get; set; }
    }
}
