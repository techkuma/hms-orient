﻿using System;

namespace Booking.Domain.Models
{
    public class ReservationCancellationPolicy : Entity
    {
        public Guid ReservationID { get; set; }
        public string CancellationPolicyRefID { get; set; }
        public string CancellationPolicyName { get; set; }
        public string CancellationDescription { get; set; }
        public string JsonValue { get; set; }
        public bool CanCancel { get; set; }
        public bool NonRefundable { get; set; }
        public int HourThresholdBeforeReservation { get; set; }
        public decimal? CancelPenaltyAmount { get; set; }
        public decimal? CancelPenaltyPercent { get; set; }
        public int CancelPenaltyNumberNights { get; set; }

        public virtual Reservation Reservation { get; set; }
    }
}
